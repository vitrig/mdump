#define TX 1
#define RX 2

struct BitRate{
	time_t begin;
	uint64_t bytesBase;
	uint64_t CurrBitrate;
	int type;
};

struct PckRate{
	time_t begin;
	uint64_t packetsBase;
	uint64_t CurrPktrate;
	int type;
};

struct NetL{
	struct rtnl_link * link;
	struct nl_sock * sock;
	struct BitRate ** BrateTX;
	struct PckRate ** PrateTX;
	struct BitRate ** BrateRX;
	struct PckRate ** PrateRX;
	int intfCount;
	char ** intf;
	int * baseValsTX;
	int * baseValsRX;
	int * BbaseValsTX;
	int * BbaseValsRX;
	int * dropBaseRX;
	int * dropBaseTX;
	void ( *beginLoop)(struct NetL * self,int intf); 
	void (*endLoop)(struct NetL * self);
	uint64_t (*tx)(struct NetL * self,int intf);
	uint64_t (*rx)(struct NetL * self,int intf);
	uint64_t (*Btx)(struct NetL * self, int intf);
	uint64_t (*Brx)(struct NetL * self, int intf);
	uint64_t (*Drx)(struct NetL * self, int intf);
	uint64_t (*Dtx)(struct NetL * self, int intf);
	uint64_t (*getBrate)(struct NetL * self,int intf,int type); 
	uint64_t (*getPrate)(struct NetL * self, int intf,int type);      	
};

struct NetL * CreateNetL();
void DestroyNetL(struct NetL * NET);
//int checkIntfNumber;
void bye(int sig);
void checkSU();
