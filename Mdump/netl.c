#include <netlink/netlink.h>
#include <netlink/route/link.h>
#include <netlink/route/tc.h>
#include <ncurses.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <signal.h>
#include "sigstruct.h"
#include "aux.h"
#include "traffic-visualizer.h"
#include <time.h>

int main (int argc,char * argv[])
{

	checkSU();
	struct NetL * NET = CreateNetL();
	memset(&sigS,0,sizeof(struct sigstruct));
	sigS.sock = NET->sock;
	sigS.NET = NET;		
	signal(SIGINT,bye);
	int ret = 0;
	srand(time(NULL));

	WINDOW * win,*MR;



	struct traffic_visualizer * vis = create_traffic_visualizer(NET->intfCount, 1, 2);
	sigS.vis =vis;
	int i = 0;
	for(;i<NET->intfCount;++i)
	{	
		vis->activate_port(vis, i,NET->intf[i]);	
	}
	i = 0;
	initscr();
	MR = newwin(80,100,0,80);
	start_color();	
	init_pair(1,COLOR_BLACK,COLOR_RED);
	init_pair(2,COLOR_BLACK,COLOR_BLUE);	
	cbreak();
	curs_set(0);
	while(1)
	{
	
		vis->render(vis);	
		win = newwin(40,62,NET->intfCount+1,1);
		sigS.win = win;
		for(i = 0; i<NET->intfCount;++i)
		{
			NET->beginLoop(NET,i);				
			mvwprintw(win,5*i,0,"%s",NET->intf[i]);
			mvwprintw(win,(5*i)+1,0,"transmitted :");
			mvwprintw(win,(5*i)+2,0,"packets: %llu bytes: %llu, dropped: %llu bitrate: %llu packetrate: %llu",

					NET->tx(NET,i),NET->Btx(NET,i),NET->Dtx(NET,i),
					NET->getBrate(NET,i,TX),NET->getPrate(NET,i,TX));
			mvwprintw(win,(5*i)+3,0,"received");
			mvwprintw(win,(5*i)+4,0,"packets: %llu bytes: %llu, dropped: %llu bitrate: %llu packetrate: %llu",
					NET->rx(NET,i),NET->Brx(NET,i),NET->Drx(NET,i),
					NET->getBrate(NET,i,RX),NET->getPrate(NET,i,RX));
			NET->endLoop(NET);
		}
		wrefresh(win);
		wrefresh(stdscr);
int k = rand() %7 +1;
init_pair(3,k,COLOR_BLACK);
wattron(MR,COLOR_PAIR(3));
if(argc>1)
{
if(!(strcmp(argv[1],"-priest")))
{
mvwprintw(MR,0,0,"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKOkkkkOOO0NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMMMMMMMMMMMMN0ol:;,'';;;;:::okKNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMMMMMMMXOkxc'  .,'.........';cccKWMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMMMW0o,.',..     .....    ..';:;.;dONMMMMMMMMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMWk'    ...        ....  .....'.    ;XMMMMMMMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMW0:                                   .oXMMMMMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMWk.                        ....'','.'.   ,KMMMMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMN'                     .;:::ccclllc;,'..  'XMMMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMX.                 .,,';loddxkkkkOkxl:;'.. cMMMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMW;        ..'''...'coollodxxkkkkkOOOkdl:,'.;WMMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMO    .....';::c::cooooooodxxxxkkOO0Okdoc;,,OMMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMK.  ......',;:cllllllloodxxkkkkkOO0OOkdlc;,oMMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMX.  .......',:cloolccllodxxxxxxxk0000Okoc:;lWMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMx  .....'',,;:cllc:c::::;;,,;:cldxkO00kl:;;dWMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMX. ....',,;;;,;;::;:;,..   ..',;;codOKKxc:;lokWMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMM: .............,;:ool,.  ....';::loxO0OdodxdlkMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMo  .....       .':oxdc:,....',:ldkOO0OOkxooxddWMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMO  ...     ......'coddooolcccldxkO000Okxxd;:oxWMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMX. ... ...........;lxxkkOkddddxxkOOOOkxxxkl:okWMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMW; ....''',;;;,...':dxxkxdlclloddxkkkkkxxxxkOXMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMMo...',;;:cllc;...';ldxddoc;,,;:clodxxxxxddkXMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMMK,...',;;:c::,'..',:cll:ccc;'...,;ldddddkKWMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMMMx'...',,;;'.... ...'',;:cccc;'...,cloodXMMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMMMNl....',,'......  ...';::;:::;,;;;:clooXMMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMMMMNl...'''.............',;clooolcc:cllooKMMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMMMMMNo...','...........',;clloddoolccllllKMMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMMMMMMNl...',,'''.....''',:loddddoolllllclOMMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMMMMMMMX;..'','''...'',;;:coddddoollclccloOMMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMMMMMMMMKl'.'''......';:::cllcccccccccclooOMMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMMMMMMMMMMk ..........',,;;;,,,;::cc::coodkNMMMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMMMMMMMMNx:................'',,;:::::lloodx0NWMMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMMMMMMMXc'.  ..  ..........'',,,;;:clloodx:dNXWMMMMMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMMMMMMNo:;;  ....   ..........',;ccloodddo'dWWWKKWWMMMMMMMMMMMM\n\
MMMMMMMMMMMMMMMMMMMMOclcl; ......   .....',,;:ccloodddo:dNWMMN0XNWNNWNWMMMMMM\n\
MMMMMMMMMMMWNNXXK0KOoollll:'............',,;::clodddollkNNWWWKKNWWNXNWWWMMWWN\n\
MMMMMMWWNX0kxddkkkkxolllolool:,.''''...'',;:cloooolldONNXXNNXONWWNXXNMMWWMWN0\n\
XKK00000OOOkxdooodxxdollcclooxxdc;,,,;;;:::ccooocl0KXNXXXXXKKNWNWNXXNNWWWWWWW\n\
NNNNNXXNNNXXXXXK00KKXXK00O00KKXNNX0OOOOO00000XK0OkXWWWNWWWWNWWWWWWWWWWWWWWWWW\n\
MMMMMMMMMMMMMMMMMMWXKKXNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\n\
MM0...OMMKl..;OW0;.    ..OMMMc..OMMMMMk....kMMMMMO.....',:o0WMMc..........dMM\n\
MMO   xO;  .dNMM,  :0K0OdXMMM,  kMMMMO.    .0MMMMk  .kkdc.  :NM0kxxxx,   '0MM\n\
MMO   .  'xWMMMM:   ':oxKMMMM,  kMMMX.  oc  .XMMMk  ,MMMMWc  lMMMMW0;  .kWMMM\n\
MMO      cKWMMMMNkl:,.   ;NMM,  kMMMl  ,NX,  lMMMk  ,MMMMMd  ;MMMK:  .dNMMMMM\n\
MMO   oc   ;KMMMKNMMMWl   0MM,  kMMO   ....   OMMk  ,MMMNk.  kMWd   ;XMMMMMMM\n\
MMO   kW0l. .c0W,..','  .lWMM,  kMK. .okkkko. .KMk  .,,'. .,kMMl   .,,,,,;kMM\n\
MMNdddXMMMNkddxXN0kxxdk0NMMMMOddXM0dd0MMMMMM0dd0MXddddxxOKNMMMMOddddddddddKMM\n\
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\n");

wattroff(MR,COLOR_PAIR(3));
}
}
		wrefresh(MR);
		delwin(win);
	}
	endwin();

	return ret;
}
