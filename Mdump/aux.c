#include <stdio.h>
#include <netlink/netlink.h>
#include <netlink/route/link.h>
#include <netlink/route/tc.h>
#include <ifaddrs.h>
#include <unistd.h>
#include <ncurses.h>
#include <sys/types.h>
#include "sigstruct.h"
#include <time.h>
#include "aux.h"
#include "traffic-visualizer.h"

void checkSU()
{
	uid_t uid=getuid();
	if(uid != 0)
	{
		printf("You must have root priviledges to run this program! Consider using su or sudo. Program will terminate.\n");
		exit(1);
	}
}

int  checkIntfNumber()
{
	int i=0;
	struct ifaddrs * tmp, * addrs;
	getifaddrs(&addrs);
	tmp = addrs;
	while (tmp)
	{
		if (tmp->ifa_addr && tmp->ifa_addr->sa_family == AF_PACKET)
		{
			++i;					
		}
		tmp = tmp->ifa_next;	
	}
	freeifaddrs(addrs);
	return i;
}

void DestroyNetL(struct NetL * NET)
{
	free(NET->baseValsTX);
	free(NET->baseValsRX);
	free(NET->BbaseValsTX);
	free(NET->BbaseValsRX);
	free(NET->dropBaseRX);
	free(NET->dropBaseTX);
	int i =0;
	for(;i<NET->intfCount;++i)
	{
		free(NET->intf[i]);
		free(NET->BrateTX[i]);
		free(NET->PrateTX[i]);
		free(NET->BrateRX[i]);
		free(NET->PrateRX[i]);
	}
	free(NET->intf);
	free(NET->BrateTX);
	free(NET->PrateTX);
	free(NET->BrateRX);
	free(NET->PrateRX);
	free(NET);
}
void bye(int sig)
{	
	delwin(sigS.win);
	rtnl_link_put(sigS.link);
	nl_socket_free(sigS.sock);
	DestroyNetL(sigS.NET);	
	delwin(stdscr);
	destroy_traffic_visualizer(sigS.vis);
	endwin();
	exit(1);
}

uint64_t tx(struct NetL * self, int intf)
{
	return (rtnl_link_get_stat(self->link,RTNL_LINK_TX_PACKETS)-(self->baseValsTX[intf]));
}

uint64_t rx(struct NetL * self, int intf)
{
	return (rtnl_link_get_stat(self->link,RTNL_LINK_RX_PACKETS)-(self->baseValsRX[intf]));
}

uint64_t Btx(struct NetL * self, int intf)
{
	
	return (rtnl_link_get_stat(self->link,RTNL_LINK_TX_BYTES)-(self->BbaseValsTX[intf]));
}

uint64_t Brx(struct NetL * self, int intf)
{
	return (rtnl_link_get_stat(self->link,RTNL_LINK_RX_BYTES)-(self->BbaseValsRX[intf]));
}
uint64_t Drx(struct NetL *self, int intf)
{
	return (rtnl_link_get_stat(self->link,RTNL_LINK_RX_DROPPED) -(self->dropBaseRX[intf]));
}

uint64_t Dtx(struct NetL * self, int intf)
{
	return (rtnl_link_get_stat(self->link,RTNL_LINK_TX_DROPPED) - (self->dropBaseTX[intf]));
}

void beginLoop(struct NetL * self, int intf)
{
	rtnl_link_get_kernel(self->sock,0,self->intf[intf],&(self->link));
}

void endLoop(struct NetL * self)
{
	rtnl_link_put(self->link);
}

uint64_t getBrate(struct NetL * self, int intf, int type)
{
	struct BitRate ** Brate;
	if(type==TX)
	{
		Brate = self->BrateTX;	
	}
	else if(type==RX)
	{
		Brate = self->BrateRX;
	}
	if(time(NULL) - Brate[intf]->begin>=1)
	{
		Brate[intf]->begin = time(NULL);
		uint64_t diff = 0;
		if(type==RX)
		{	
			diff =   (self->Brx(self,intf) - Brate[intf]->bytesBase );
			Brate[intf]->bytesBase = self->Brx(self,intf);
			sigS.vis->set_rx_bitrate(sigS.vis,intf,diff);
		}
		else if(type==TX)	
		{
			diff =   (self->Btx(self,intf) - Brate[intf]->bytesBase );
			Brate[intf]->bytesBase = self->Btx(self,intf);
			sigS.vis->set_tx_bitrate(sigS.vis,intf,diff);	
		}
		Brate[intf]->CurrBitrate = diff;
		return diff;
	}
	return 8*Brate[intf]->CurrBitrate;
}

uint64_t getPrate(struct NetL * self, int intf,int type)
{	
	struct PckRate ** Prate;
	if(type==TX)
	{
		Prate = self->PrateTX;	
	}
	else if(type==RX)
	{
		Prate = self->PrateRX;
	}

	if(time(NULL) - Prate[intf]->begin>=1)
	{	
		Prate[intf]->begin = time(NULL);
		uint64_t diff = 0;
		
		if(type==RX)
		{
			diff =   (self->rx(self,intf) - Prate[intf]->packetsBase );
			Prate[intf]->packetsBase = self->rx(self,intf);	
		}
		else if(type==TX)
		{
			diff =   (self->tx(self,intf) - Prate[intf]->packetsBase );
			Prate[intf]->packetsBase = self->tx(self,intf);	
		}
		Prate[intf]->CurrPktrate = diff;
		return diff;
	}
	return Prate[intf]->CurrPktrate;
}

struct NetL * CreateNetL() 
{
	struct NetL *  NET = malloc(sizeof(struct NetL));
	memset(NET, 0, sizeof(struct NetL));
	NET->sock = nl_socket_alloc();
	int ret=0;
	if(NET->sock == 0)
	{
		printf("Failedto alloc netlink socket\n");
		return 0;	
	}

	ret = nl_connect(NET->sock,NETLINK_ROUTE);
	if(ret!=0)
	{
		printf("Failed to connect to kernel\n");	
		return 0;
	}

	ret = nl_socket_add_membership(NET->sock,RTNLGRP_LINK);
	if(ret != 0)
	{
		printf("Joining group failed\n");
		return 0;	
	}
	NET->rx = rx;
	NET->tx = tx;
	NET->Btx = Btx;
	NET->Brx = Brx;
	NET->Dtx = Dtx;
	NET->Drx = Drx;
	NET->beginLoop = beginLoop;
	NET->endLoop = endLoop;
	NET->getBrate = getBrate;
	NET->getPrate = getPrate;
	NET->intfCount = 0;
	
	struct ifaddrs *tmp,* addrs;
/*	while (tmp)
	{
		if (tmp->ifa_addr && tmp->ifa_addr->sa_family == AF_PACKET)
		{					
			++(NET->intfCount); 
		}
		tmp = tmp->ifa_next;
	}*/
	NET->intfCount =  checkIntfNumber();
	NET->intf = malloc((NET->intfCount)*sizeof(char *));
	NET->baseValsTX = malloc((NET->intfCount)*sizeof(int ));
	NET->baseValsRX = malloc((NET->intfCount)*sizeof(int ));
	NET->dropBaseRX = malloc((NET->intfCount)*sizeof(int));
	NET->dropBaseTX = malloc((NET->intfCount)*sizeof(int));
	NET->BbaseValsTX = malloc((NET->intfCount)*sizeof(int));
	NET->BbaseValsRX = malloc((NET->intfCount)*sizeof(int));
	NET->BrateRX = malloc((NET->intfCount)*sizeof(struct BitRate *));
	NET->PrateRX = malloc((NET->intfCount)*sizeof(struct PckRate *));
	NET->BrateTX = malloc((NET->intfCount)*sizeof(struct BitRate *));
	NET->PrateTX = malloc((NET->intfCount)*sizeof(struct PckRate *));
	memset(NET->intf,0, (NET->intfCount)*sizeof(char *));
	memset(NET->baseValsTX,0, (NET->intfCount)*sizeof(int));
	memset(NET->baseValsRX,0, (NET->intfCount)*sizeof(int));	
	memset(NET->dropBaseRX,0, (NET->intfCount)*sizeof(int));
	memset(NET->dropBaseTX,0, (NET->intfCount)*sizeof(int));
	memset(NET->BbaseValsTX,0, (NET->intfCount)*sizeof(int));
	memset(NET->BbaseValsRX,0, (NET->intfCount)*sizeof(int));
	memset(NET->BrateRX, 0, (NET->intfCount)*sizeof(struct BitRate *));
	memset(NET->PrateRX, 0 ,(NET->intfCount)*sizeof(struct PckRate *)); 
	memset(NET->BrateTX, 0, (NET->intfCount)*sizeof(struct BitRate *));
	memset(NET->PrateTX, 0 ,(NET->intfCount)*sizeof(struct PckRate *));
	int intfNum = 0;
	getifaddrs(&addrs);
	tmp = addrs;
	while(tmp)
	{
		if (tmp->ifa_addr && tmp->ifa_addr->sa_family == AF_PACKET)
		{					
			NET->intf[intfNum] = malloc((strlen(tmp->ifa_name)*sizeof(char))+1);
			strcpy(NET->intf[intfNum],tmp->ifa_name);
			NET->beginLoop(NET,intfNum);
			NET->baseValsTX[intfNum] = rtnl_link_get_stat(NET->link,RTNL_LINK_TX_PACKETS);
			NET->baseValsRX[intfNum] = rtnl_link_get_stat(NET->link,RTNL_LINK_RX_PACKETS);
			NET->BbaseValsTX[intfNum] = rtnl_link_get_stat(NET->link,RTNL_LINK_TX_BYTES);
			NET->BbaseValsRX[intfNum] = rtnl_link_get_stat(NET->link,RTNL_LINK_RX_BYTES);
			NET->dropBaseTX[intfNum] = rtnl_link_get_stat(NET->link,RTNL_LINK_TX_DROPPED);
			NET->dropBaseRX[intfNum] = rtnl_link_get_stat(NET->link,RTNL_LINK_RX_DROPPED);
			NET->BrateRX[intfNum] = malloc(sizeof(struct BitRate));
			NET->PrateRX[intfNum] = malloc(sizeof(struct PckRate));
			NET->BrateTX[intfNum] = malloc(sizeof(struct BitRate));
			NET->PrateTX[intfNum] = malloc(sizeof(struct PckRate));	
			memset(NET->BrateRX[intfNum],0 , sizeof(struct BitRate));
			memset(NET->PrateRX[intfNum], 0 , sizeof(struct PckRate));
			memset(NET->BrateTX[intfNum],0 , sizeof(struct BitRate));
			memset(NET->PrateTX[intfNum], 0 , sizeof(struct PckRate));
			NET->BrateRX[intfNum]->begin  = time(NULL);
			NET->PrateRX[intfNum]->begin = time(NULL);
			NET->BrateTX[intfNum]->begin  = time(NULL);
			NET->PrateTX[intfNum]->begin = time(NULL);
			NET->endLoop(NET);
			intfNum++;	
		}
		tmp = tmp->ifa_next;
	}
	freeifaddrs(addrs);

	return NET;
}

